import { EmplacementsComponent } from './pages/emplacements/emplacements.component';
import { JustificatifsComponent } from './pages/justificatifs/justificatifs.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InformationsComponent } from './pages/informations/informations.component';
import { MarcheAuxPucesComponent } from './pages/marche-aux-puces/marche-aux-puces.component';
import { ValidationComponent } from './pages/validation/validation.component';
import { PaiementComponent } from './pages/paiement/paiement.component';

const routes: Routes = [
  {path: 'marche_aux_puces', component: MarcheAuxPucesComponent},
  {path: 'informations', component: InformationsComponent},
  {path: 'justificatifs', component: JustificatifsComponent},
  {path: 'emplacements', component: EmplacementsComponent},
  {path: 'paiement', component: PaiementComponent},
  {path: 'validation', component: ValidationComponent},
  {path: '**', redirectTo: 'marche_aux_puces'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes) ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
