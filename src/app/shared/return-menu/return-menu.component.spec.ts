import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReturnMenuComponent } from './return-menu.component';

describe('ReturnMenuComponent', () => {
  let component: ReturnMenuComponent;
  let fixture: ComponentFixture<ReturnMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReturnMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReturnMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
