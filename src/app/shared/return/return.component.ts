import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-return',
  templateUrl: './return.component.html',
  styleUrls: ['./return.component.scss']
})
export class ReturnComponent {

  constructor(private router: Router) {
  }
  @Input() page: string;
  @Input() public return: string;
  public goToReturnRoute() {
    this.router.navigate([this.return]);
  }
}
