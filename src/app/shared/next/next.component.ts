import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-next',
  templateUrl: './next.component.html',
  styleUrls: ['./next.component.scss']
})
export class NextComponent {

  constructor(private router: Router) {
  }
  @Input() page: string;
  @Input() public next: string;


  public goToNextRoute() {

    this.router.navigate([this.next]);

  }
}
