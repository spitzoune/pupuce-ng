import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HeaderComponent} from './header/header.component';
import {StepperComponent} from './shared/stepper/stepper.component';
import {NavigationButtonsComponent} from './shared/navigation-buttons/navigation-buttons.component';
import {TitleComponent} from './shared/title/title.component';
import {FinishComponent} from './shared/finish/finish.component';
import {MarcheAuxPucesComponent} from './pages/marche-aux-puces/marche-aux-puces.component';
import {InformationsComponent} from './pages/informations/informations.component';
import {JustificatifsComponent} from './pages/justificatifs/justificatifs.component';
import {EmplacementsComponent} from './pages/emplacements/emplacements.component';
import {ValidationComponent} from './pages/validation/validation.component';
import {SelectionnerEmplacementComponent} from './pages/emplacements/selectionner-emplacement/selectionner-emplacement.component';
import {MapEmplacementComponent} from './pages/emplacements/map-emplacement/map-emplacement.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {PaiementComponent} from './pages/paiement/paiement.component';
import {ReturnComponent} from './shared/return/return.component';
import {NextComponent} from './shared/next/next.component';
import {NewRegistrationComponent} from './shared/new-registration/new-registration.component';
import {ReturnMenuComponent} from './shared/return-menu/return-menu.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule, MatInputModule} from '@angular/material';


@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        StepperComponent,
        NavigationButtonsComponent,
        TitleComponent,
        FinishComponent,
        MarcheAuxPucesComponent,
        InformationsComponent,
        JustificatifsComponent,
        EmplacementsComponent,
        ValidationComponent,
        SelectionnerEmplacementComponent,
        MapEmplacementComponent,
        PaiementComponent,
        ReturnComponent,
        NextComponent,
        NewRegistrationComponent,
        ReturnMenuComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MatInputModule,
        MatIconModule,
        MatFormFieldModule,
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
